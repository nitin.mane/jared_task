function output = regnvar(LHS,rf,RHS,stat,header,var,varNames,orientation)
%   This function takes in the left-hand side regression variable "LHS"; a
%   right-hand side regression variable(s) "RHS"; a t-stat/p-value identification
%   variable "stat"; a header yes/no variable, "header"; a variable name(s) 
%   yes/no variables, "var"; variable name(s) string vector "varNames"; and
%   an orientation variable "orientation".

stats=regstats(LHS-rf,RHS);

if strcmp(header,'Yes')||strcmp(header,'yes')||strcmp(header,'Y')|| ... 
        strcmp(header,'y')==1

    if strcmp(stat,'t-stat')==1
        
        if strcmp(var,'Yes')||strcmp(var,'yes')||strcmp(var,'Y')||strcmp(var,'y')
            if (varName == 2)
                output={'Alpha (bps)' 't-stat' varNames{1} 't-stat' varNames{2} 't-stat' 'R-squared'; ...
                stats.tstat.beta(1)*1200 stats.tstat.t(1) stats.tstat.beta(2) ...
                stats.tstat.t(2) stats.tstat.beta(3) stats.tstat.t(3) ...
                stats.adjrsquare};
           
            else
                 output={'Alpha (bps)' 't-stat' 'Beta1' 't-stat' 'Beta2' 't-stat' 'R-squared'; ...
                  stats.tstat.beta(1)*1200 stats.tstat.t(1) stats.tstat.beta(2) ...
                  stats.tstat.t(2) stats.tstat.beta(3) stats.tstat.t(3) ...
                  stats.adjrsquare};
            
                
            if (varName == 3)
                output={'Alpha (bps)' 'p-value' varNames{1} 'p-value' varNames{2} 'p-value' varNames{3} 'p-value' 'R-squared'; ...
                stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
                stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
                stats.tstat.beta(4) stats.tstat.pval(4) stats.adjrsquare};
            
            else
                    
                output={'Alpha (bps)' 'p-value' varNames{1} 'p-value' varNames{2} 'p-value' varNames{3} 'p-value' 'R-squared'; ...
                stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
                stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
                stats.tstat.beta(4) stats.tstat.pval(4) stats.adjrsquare};
            
             if (varName == 4)  
                  output={'Alpha (bps)' 'p-value' varNames{1} 'p-value' varNames{2} 'p-value' varNames{3} 'p-value' varNames{4} 'p-value' 'R-squared'; ...
                  stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
                  stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
                  stats.tstat.beta(4) stats.tstat.pval(4) stats.tstatbeta(5) 
                  stats.tstat.pval(5) stats.adjrsquare}
              
             else 
                
                  output={'Alpha (bps)' 't-stat' varNames{1} 't-stat' varNames{2} 'p-value' varNames{3} 'p-value' varNames{4} 't-stat' 'R-squared'; ...
                  stats.tstat.beta(1)*1200 stats.tstat.t(1) stats.tstat.beta(2) ...
                  stats.tstat.t(2) stats.tstat.beta(3) stats.tstat.t(3) ...
                  stats.tstat.beta(4) stats.tstat.pval(4) stats.tstatbeta(5) 
                  stats.tstat.pval(5) stats.adjrsquare}
                 
            end
            end 
            end
        end
        
    elseif strcmp(stat,'p-value')==1
        
        if strcmp(var,'Yes')||strcmp(var,'yes')||strcmp(var,'Y')||strcmp(var,'y')
            if (varNames == 2) 
            output={'Alpha (bps)' 'p-value' varNames{1} 'p-value' varNames{2} 'p-value' 'R-squared'; ...
                stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
                stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
                stats.adjrsquare};
        else
            output={'Alpha (bps)' 'p-value' 'Beta1' 'p-value' 'Beta2' 'p-value' 'R-squared'; ...
                stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
                stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
                stats.adjrsquare};
            
            if (valNames == 3) 
                output={'Alpha (bps)' 'p-value' varNames{1} 'p-value' varNames{2} 'p-value' 'R-squared'; ...
                stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
                stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
                stats.adjrsquare};
        else
            output={'Alpha (bps)' 'p-value' 'Beta1' 'p-value' 'Beta2' 'p-value' 'R-squared'; ...
                stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
                stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
                stats.tstat.beta(4) stats.tstat.pval(4) stats.adjrsquare};
            
            if (valNames == 4) 
                output={'Alpha (bps)' 'p-value' varNames{1} 'p-value' varNames{2} 'p-value' 'R-squared'; ...
                stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
                stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
                stats.tstat.beta(4) stats.tstat.pval(4) stats.tstat.beta(5) 
                stats.tstat.pval(5) stats.adjrsquare};
        else
            output={'Alpha (bps)' 'p-value' 'Beta1' 'p-value' 'Beta2' 'p-value' 'R-squared'; ...
                stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
                stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
                stats.tstat.beta(4) stats.tstat.pval(4) stats.tstat.beta(5) 
                stats.tstat.pval(5) stats.adjrsquare};
            
            end
            end
            end
        end
      
    else
        error('Re-check formula input.')
    end
    
else
    
    if strcmp(stat,'t-stat')==1
        output={stats.tstat.beta(1)*1200 stats.tstat.t(1) stats.tstat.beta(2) ...
            stats.tstat.t(2) stats.tstat.beta(3) stats.tstat.t(3) ...
            stats.adjrsquare};
    elseif strcmp(stat,'p-value')==1
        output={stats.tstat.beta(1)*1200 stats.tstat.pval(1) stats.tstat.beta(2) ...
            stats.tstat.pval(2) stats.tstat.beta(3) stats.tstat.pval(3) ...
            stats.adjrsquare};
    else
        error('Re-check formula input.')
    end
    
end

if strcmp(orientation,'Horizontal')||strcmp(orientation,'horizontal')|| ...
        strcmp(orientation,'H')||strcmp(orientation,'h')==1;
  
    output;
    
else
    
    output=output';
    
end

end