%
prompt = 'Enter the stat for significance of coefficient (t-stat/p-value) :';
stat = input(prompt,'s');
prompt = 'Enter the number of variables: ';
num = str2num(input(prompt,'s'));


% stat='p-value';
prompt = 'Enter the header status as yes or no (y/n): ';
header = input(prompt,'s');
% header='y';
prompt = 'Enter the var status as yes or no(y/n): ';
var= input(prompt,'s');
% var='y';
prompt = 'Enter the orientation of output as horizontal(h) or vertical(v) as (h/v): ';
orientation = input(prompt,'s');
% prompt = 'Enter the LHS : ';
% LHS_enter = (input(prompt,'s'));
LHS=cell2mat(RegressionExamples(2:end,2));
% prompt = 'Enter the RHS : ';
% RHS_enter = (input(prompt,'s'));
RHS=cell2mat(RegressionExamples(2:end,4:num+3));
% prompt = 'Enter the rf : ';
% % rf_enter = (input(prompt,'s'));
rf=cell2mat(RegressionExamples(2:end,3));
%
% prompt = 'Enter the variable name : ';
% varNames_enter= cellstr(input(prompt,'s'));
varNames= cellstr(RegressionExamples(1,:));
varNames= varNames(4:(num+3));


prompt = 'Press 1 for regnvar or Press 2 for regnvarxrf?? ';
selection = str2num(input(prompt,'s'));


prompt = 'Do you want to get the output? (Y/n) : ';
answer = input(prompt,'s');

if strcmp(answer,'Yes')||strcmp(answer,'yes')||strcmp(answer,'Y')|| ...
        strcmp(answer,'y')==1
    if selection==1
        output= regNvar(LHS,rf,RHS,stat,header,var,varNames,orientation);
        disp(output);
    elseif selection==2
        output_num= regnvarxrf(LHS,RHS,stat,header,var,varNames,orientation);
        disp(output);
    end
    
elseif strcmp(answer,'NO')||strcmp(answer,'no')||strcmp(answer,'N')|| ...
        strcmp(answer,'n')==1
    % output= regNvar(LHS,rf,RHS,stat,header,var,varNames,orientation)
    
end
